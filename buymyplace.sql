-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2012 at 05:12 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `buymyplace`
--

-- --------------------------------------------------------

--
-- Table structure for table `buymy_agent_reviews`
--

DROP TABLE IF EXISTS `buymy_agent_reviews`;
CREATE TABLE `buymy_agent_reviews` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `review_points` int(11) NOT NULL,
  `review_text` varchar(1000) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_agent_reviews`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_images`
--

DROP TABLE IF EXISTS `buymy_images`;
CREATE TABLE `buymy_images` (
  `id` int(11) NOT NULL auto_increment,
  `property_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_images`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_property`
--

DROP TABLE IF EXISTS `buymy_property`;
CREATE TABLE `buymy_property` (
  `id` int(11) NOT NULL auto_increment,
  `owner_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(50) NOT NULL,
  `bedrooms` varchar(255) NOT NULL,
  `bathrooms` varchar(255) NOT NULL,
  `beds` varchar(255) NOT NULL,
  `parking` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_property`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_property_type`
--

DROP TABLE IF EXISTS `buymy_property_type`;
CREATE TABLE `buymy_property_type` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_property_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_users`
--

DROP TABLE IF EXISTS `buymy_users`;
CREATE TABLE `buymy_users` (
  `id` int(11) NOT NULL auto_increment,
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(50) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_users`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_user_property`
--

DROP TABLE IF EXISTS `buymy_user_property`;
CREATE TABLE `buymy_user_property` (
  `property_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buymy_user_property`
--


-- --------------------------------------------------------

--
-- Table structure for table `buymy_user_type`
--

DROP TABLE IF EXISTS `buymy_user_type`;
CREATE TABLE `buymy_user_type` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `buymy_user_type`
--

